package regexps;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



public class PhoneAnalizator {
  public String pathInputFile;

  public String pathOutputFile;



  public PhoneAnalizator(String pathInputFile, String pathOutputFile) {
    super();
    this.pathInputFile = pathInputFile;
    this.pathOutputFile = pathOutputFile;
  }

  public void phoneAnalizis() {
    try {
      Path pth = Paths.get("src/source", "input_file");
      String tmp = Files.readAllLines(pth).parallelStream().reduce((s1, s2) -> s1 + s2).get();
      Pattern p = Pattern.compile("\\+([0-9]{1})\\((\\d{3})\\)\\s([0-9]{3})\\s([0-9]{2})\\s([0-9]{2})");
      Matcher m = p.matcher(tmp);
      while (m.find()) {
        String result = m.group(1)+m.group(2)+m.group(3)+m.group(4)+m.group(5);
        resultWrite(result);
      }
      // bf.close();
    } catch (IOException e) {
      e.printStackTrace();
    }

  }

  public void resultWrite(String stringResult) {
    FileWriter fw;
    try {
      fw = new FileWriter(new File(pathOutputFile), true);
      BufferedWriter bw = new BufferedWriter(fw);
      bw.write(stringResult);
      bw.newLine();
      System.out.println(stringResult);
      bw.close();
    } catch (IOException e) {
      e.printStackTrace();
    }

  }
}


